import { express } from "express";
const router = express.Router();

import {
  GetAllUsers,
  GetUser,
  UpdateUser,
  DeleteUser,
} from "../controllers/User";

// here we are not defining the function in the routes file
// defining our routes and whatever name in the endpoint it needs
// then calling the functions from the controller function
router.get("/all", GetAllUsers);
router.get("/byId/:id", GetUser);
router.put("/", UpdateUser);
router.delete("/:id", DeleteUser);

export { router as UserRoute };
