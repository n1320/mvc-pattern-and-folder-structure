// all the service functions communicate directly with the database
// in the controller we use the service functions to get the data/result of the service functions
// we can also now reuse any of these functions throughout the app

// Returns User With Id
export const QueryUserById = (id) => {
  return User.FindById(id);
};

// Returns List of Users
export const QueryListOfUsers = (condition) => {
  if (condition) {
    return User.findAll(condition).exec();
  }

  return User.findAll().exec();
};

export const DeleteUserByID = (id) => {
  return User.remove({ _id: id }).exec();
};
