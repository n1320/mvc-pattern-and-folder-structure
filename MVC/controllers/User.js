import {
  QueryListOfUsers,
  QueryUserById,
  DeleteUserById,
} from "../service/UserTable";

export const GetAllUsers = (req, res) => {
  const userList = QueryListOfUsers();

  // DO SOMETHING WITH THE USER LIST OR JUST RETURN IT
  // Where logic will be written for updating/mutating/doing anything with the data we got from the database table
  // We then return the data as a json back to the route
  return res.json(userList);
};

export const GetUser = (req, res) => {
  const userId = req.params.id;
  const user = QueryUserById(userId);

  // DO SOMETHING WITH THE USER LIST OR JUST RETURN IT
  return res.json(user);
};

export const DeleteUserById = (req, res) => {
  const userId = req.params.id;
  const user = DeleteUserById(userId);

  // DO SOMETHING WITH THE USER LIST OR JUST RETURN IT
  return res.json(user);
};

export const UpdateUser = (req, res) => {};
