import { express } from "express";
const router = express.Router();

// All of the logic in this api is in one file - in here we are currently dealing with everything
// File soon becomes unmaintainable - if you have hundreads of endpoints it will be difficult to maintain/keep the api structure this way
// dealing with what the user sends
router.get("/all", (req, res) => {
  const userList = User.findAll().exec();
  res.json(userList);
});

router.get("/byId/:id", (req, res) => {
  // dealing with calling the db directly
  const user = User.findById(req.params.id).exec();
  // dealing with returning the data
  res.json(user);
});

router.post("/create", (req, res) => {
  const User = req.body;
  User.save((err, user) => {
    if (err) {
      res.send(err);
    } else {
      res.json(user);
    }
  });
  res.json(req.body);
});
